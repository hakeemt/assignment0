extends Control

func _on_PlayAgain_pressed():
	get_tree().change_scene_to_file("res://Level/game_level.tscn")
	get_tree().reload_current_scene()
	
func _on_Quit_pressed():
	get_tree().quit()
