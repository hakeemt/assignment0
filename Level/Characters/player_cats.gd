extends CharacterBody2D

@export var move_speed : float = 100
@export var starting_direction : Vector2 = Vector2(0, 1)

@onready var animation_tree = $AnimationTree
@onready var state_machine = animation_tree.get("parameters/playback")

# parameters/idle/blend_position 
func _ready():
	update_animation_parameters(starting_direction)
	

func _physics_process(_delta):
	# get input direction
	var input_direction = Vector2(
		Input.get_action_strength("right") - Input.get_action_strength("left"),
		Input.get_action_strength("down") - Input.get_action_strength("up")
	)
	
	update_animation_parameters(input_direction)
	print(input_direction)
	
	# update velocity
	velocity = input_direction * move_speed
	
	
	# Move and slide function uses velocity of character body to move character on map
	move_and_slide()

func update_animation_parameters(move_input : Vector2):
	# don't change animation parameters if there is no move input
	if(move_input != Vector2.ZERO):
		animation_tree.set("parameters/idle/blend_position", move_input)
		animation_tree.set("parameters/walk/blend_position", move_input)


func pick_new_state():
	if(velocity != Vector2.ZERO):
		state_machine.travel("Walk")
	else:
		state_machine.travel("Idle")
	
	


func _on_area_2d_body_exited(body):
	pass # Replace with function body.


func _on_area_2d_body_entered(body):
		get_tree().change_scene_to_file("res://Level/you_win.tscn")
